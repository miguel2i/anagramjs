let express = require('express');
let app = express();
let http = require("http").Server(app);
let fs = require("fs");
let io = require('socket.io')(http);

function shuffle(array) {
    let m = array.length,
        t, i;

    // While there remain elements to shuffle…
    while (m) {

        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);

        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
    }
    return array;
}

let letterGame = {
    init: function() {
        this.loadWordList();
    },
    loadWordList: function() {
        fs.readFile("wordlist.txt", "utf8", function(err, data) {
            letterGame.wordlist = data.split("\n").filter(function(a) {
                return a.length > 2;
            }).sort(function(a, b) {
                if (a.length == b.length) {
                    return (a < b) ? -1 : 1;
                } else
                    return a.length - b.length;
            });
            letterGame.wordlistShuffle = shuffle(letterGame.wordlist.slice());
            console.log("chargé");
        });
    },
    choiceWord: function() {
        let word, list;

        while (true) {
            if (letterGame.wordlistShuffle.length === 0) {
                letterGame.wordlistShuffle = shuffle(letterGame.wordlist.slice());
            }
            word = letterGame.wordlistShuffle.pop();
            list = letterGame.getWordsList(word);
            if (10 < list.length && list.length < 50)
                break;
        }
        word = shuffle(word.split(""));
        return [word, list];
    },
    getWordsList: function(word) {
        let list = letterGame.wordlist.filter(function(w) {
            let test = true,
                letters = word,
                i, index;

            for (i = 0; i < w.length; i++) {
                index = letters.indexOf(w[i]);
                if (index < 0) {
                    test = false;
                    break;
                }
                letters = letters.slice(0, index) + letters.slice(index + 1);
            }
            return test;
        });
        return list;
    },
    getWord : function() {
      if(this.current && this.current.end >= Date.now()) {
        return this.current;
      }
      const duree = 5 * 60 * 1e3;

      let l = this.choiceWord();
      this.current = {
        end : Date.now() + duree,
        list: l[1],
        word: l[0],
        found: {}
      };

      setTimeout(function() {
        console.log("Word "+ letterGame.current.word +"timeout");
        letterGame.current = undefined;
        io.emit("timeout");
      }, duree);

      return this.current;
    }
};

letterGame.init();

ang = express();

ang.get("/word", function(req, res) {
    let l = letterGame.getWord();
    res.send(l);
});

ang.use(express.static("public"));
ang.use("/bower_components", express.static("bower_components"));

app.use(ang);

io.on('connection', function(socket){
  socket.on("found", function(data) {
    if(! letterGame.current)
      return;

    if(letterGame.current.found[data] === undefined) {
      letterGame.current.found[data] = 1;
    }
    socket.broadcast.emit("found", data);
  });

});

console.log("Serveur demarre");
http.listen(8000);
