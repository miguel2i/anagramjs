$(function() {
    let anag = {
        end: 0,
        guess: Array(),
        letters: Array(),
        words: Array()
    };

    function initletters(letters) {
        let i, span;
        let position = $(".available_letters").offset();

        anag.guess = [];
        anag.letters = [];
        $(".letter").remove();
        for (i = 0; i < letters.length; i++) {
            span = $('<span class="letter">' + letters[i] + '</span>');
            span.data("index", i)
                .css("top", position.top)
                .css("left", position.left + 30 * i);
            anag.letters[i] = letters[i];
            $(".main").append(span);
        }
        $(".letter").click(function() {
            let index, pos;
            if ($(this).hasClass("guess")) {
                $(this).removeClass("guess");
                delete anag.guess[$(this).data("index")];
                for (index = 0; anag.letters[index] !== undefined; index++);
                anag.letters[index] = $(this).text();
                $(this).data("index", index);

                pos = $(".available_letters").offset();
                pos.left = parseInt(pos.left) + index * 30;
                $(this).animate(pos);
            } else {
                $(this).addClass("guess");
                delete anag.letters[$(this).data("index")];
                for (index = 0; anag.guess[index] !== undefined; index++);
                anag.guess[index] = $(this).text();
                $(this).data("index", index);

                pos = $(".guess_letters").offset();
                pos.left = parseInt(pos.left) + index * 30;
                $(this).animate(pos);
            }
        });
    }

    function initwords(words) {
        anag.words = words;
        $(".word").remove();

        for (let i = 0; i < words.length; i++) {
            $(".words").append('<div class="word">' +
                words[i].replace(/[a-z]/gi, "-") +
                "</div>");
        }
    }

    function otherFound(index) {
        let $e = $(".words").children().eq(index);

        $e.addClass("otherfound")
            .hide()
            .fadeIn("slow")
            .show();
    }

    $(".ok").click(function() {
        let text = anag.guess.join("");
        let index = anag.words.indexOf(text);
        let color = "red";
        let element;

        if (index >= 0) {
            socket.emit("found", index);
            element = $(".word").eq(index);
            if ($(element).text()[0] == "-") {
                color = "blue";
                $(element).text(text).hide().fadeIn(1000);
            } else
                color = "orange";
        }
        $("div.guess").css("background-color", color);

        setTimeout(function() {
            $(".guess").css("background-color", "");
        }, 200);
        $(".cancel").click();
    });

    $(".cancel").click(function() {
        $(".guess").sort(function(a, b) {
            return $(a).data("index") - $(b).data("index");
        }).trigger("click");
    });

    $(".solve").click(solve);

    function solve() {
        $(".word").each(function(index) {
            if ($(this).text()[0] == "-")
                $(this).addClass("notfound");
            $(this).text(anag.words[index]);
        });
    }

    $(document).keyup(function(event) {
        if (event.which == 13) // Enter
        {
            $(".ok").click();
            return false;
        } else if (event.which == 8) // backspace
        {
            $(".cancel").click();
            return false;
        } else {
            let key = event.key.toLowerCase();
            if ("a" <= key && key <= "z") {
                $(".letter:not(.guess)").each(function(e) {
                    if ($(this).text() == key) {
                        $(this).click();
                        return false;
                    }
                });
            }
        }
    });

    $(".new").click(function() {
        $.ajax({
                url: "word"
            })
            .done(function(data) {
                console.log(data);
                initletters(data.word);
                initwords(data.list);
                for (let n in data.found) {
                    otherFound(n);
                }
                anag.end = data.end;
                setTimeout(aff_duree, 1000);
            });
    }).click();

    function aff_duree() {
        let restant = Math.floor((anag.end - Date.now()) / 1000);
        if (anag.end === 0 || restant < 0)
            return;
        m = Math.floor(restant / 60);
        s = restant % 60;
        if (s < 10)
            s = "0" + s;
        $("#time").text(m + ":" + s);
        setTimeout(aff_duree, 1000);
    }

    let socket = io.connect();

    socket.on("found", function(data) {
            otherFound(data);
        })
        .on("timeout", function() {
            solve();
        });
});
