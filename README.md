# AnagramJS

Find more words in least than 5 min.
Using socket.io to show words found by other players
Use of jquery, node, express, socket.io

Install :
`npm install`

Usage:
`npm start`

Goto http://localhost:8000/
